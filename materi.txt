GIT

[
    git clone https://gitlab.com/yuditjhia85/yudigae02.git  => untuk kloning master branch dari remote server
    git clone <remote_server> 
]

atau

[
    git init -> untuk inisialiasi git client yang kosong
    git remote add origin git@gitlab.com:yuditjhia85/yudigae0201.git
    git remote add <nama_remote> <alamat_remote>
]


git add . => untuk menambahkan file yang new file, modified, deleted ke git client
. => start from current directory

git commit -m "ubah file1" => untuk save secara local perubahan terhadap file-file yang kita ubah dalam satu dir git 
git commit -m <pesan commit>
-m => commit with message

git push -u origin master  => untuk menulis / save files ke remote server (secara remote)
git push -u <nama_remote> <nama_branch_tujuan>

git pull origin master => untuk membaca file dari remote, ditulis ke local 
git pull <nama_remote> <nama_branch_sumber>

git checkout -b branch-a => untuk membuat sekaligus mengganti posisi current branch
git checkout -b <nama_new_branch>

git checkout branch-a => kalau branch sudah ada, tinggal ganti saja
git checkout <nama_existing_branch>

git fetch -> mengupdate struktur dari remote server

git checkout master => ke posisi yang mau di merge (destination merge)
git merge branch-d => sumber branch yang mau di merge (source branch) 
(paling beresiko)

git stash => membalikkan posisi commit ke sebelumnya


contoh git branch use case:
saya bikin aplikasi A -> customer A
- dibuat tersendiri oleh programmer Bain (posisi development) => branch master/development
- dinaikkan ke posisi staging (di test user)  { => branch staging
    -konfigurasi bisa berubah
    -perbaikan koding yang salah
}
- dinaikkan ke production => branch production-1 {
    fix, tidak ada perubahan
}
- dinaikkan ke production => branch production-2 {
    fix, tidak ada perubahan
}
dev => staging => production-1 => staging => development


saya bikin aplikasi A -> customer B
